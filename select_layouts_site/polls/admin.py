from django.contrib import admin
from django.utils.functional import curry

from .models import Graph, Layout, AlgorithmLayout


class LayoutBufferInline(admin.StackedInline):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__alg = AlgorithmLayout.objects.all()

    model = Layout
    exclude = ('svg',)

    def get_formset(self, request, obj=None, **kwargs):
        formset = super().get_formset(request, obj, **kwargs)
        if request.method == 'GET':
            initial = [{'algorithm_layout_id': a.id} for a in self.__alg]
            formset.__init__ = curry(formset.__init__, initial=initial)
        return formset

    def get_extra(self, request, obj=None, **kwargs):
        return len(self.__alg)


@admin.register(Graph)
class GraphBufferAdmin(admin.ModelAdmin):
    list_display = ('graph_xml',)
    exclude = ('id', 'graph_invariant', 'graph_zip', 'dt', 'user_id')
    inlines = [LayoutBufferInline]

    def save_model(self, request, obj, form, change):
        obj.user_id = request.user
        super().save_model(request, obj, form, change)


@admin.register(AlgorithmLayout)
class LayoutAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description')
    exclude = ('dt',)
