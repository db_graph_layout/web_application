import gzip
import logging
from collections import namedtuple
from multiprocessing import Process, Pipe, Lock, Manager
from multiprocessing.connection import Connection

from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.db import connection
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .models import Layout, Graph, User, AlgorithmLayout

logger = logging.getLogger(__name__)
logger.setLevel('INFO')


MAX_SIZE = 10_000
MIN_SIZE = 100


Layout_class = namedtuple('Layout', 'id algorithm_name svg')


class GetterSVG(object):
    def __init__(self, conn, max_size=MAX_SIZE, min_size=MIN_SIZE):
        self.lines = []
        self.data = [(1, 12, 1, 'svg'), (2, 20, 1, 'svg')]
        with conn.cursor() as cursor:
            cursor.execute(
                "select layer_id, cnt, algorithm_name, svg from v_update_queue limit %s",
                (max_size,)
            )
            self.data = cursor.fetchall()
            self.lines = []
            old_value = self.data[-1][1]
            for i in range(len(self.data) - 1, -1, -1):
                row = list(self.data[i])
                repeated_count = old_value - row[1]
                old_value = row[1]
                row[3] = row[3].tobytes()
                self.data[i] = row
                if repeated_count and i >= min_size:
                    self.lines.append((repeated_count, i + 1))

    def gen_init(self):
        for i in range(len(self.data)):
            row = self.data[i]
            self.data[i] = layout = Layout_class(row[0], row[2], gzip.decompress(row[3]).decode())
            yield layout

    def gen(self):
        if self.lines:
            for n, i in self.lines:
                for _ in range(n):
                    for row in self.data[:i]:
                        yield row
        else:
            yield from self.gen_default()

    def gen_default(self):
        for row in self.data:
            yield row


def update_queue(conn, pipe: Connection):
    while True:
        pipe.recv()
        d = GetterSVG(conn)
        pipe.send(d)


def getter(pipe: Connection):
    pipe.send(None)
    d = pipe.recv()
    while True:
        yield from d.gen_init()
        yield from d.gen()
        pipe.send(None)
        while not pipe.poll():
            yield from d.gen_default()
        d = pipe.recv()


def sender(conn):
    with conn.cursor() as cursor:
        while True:
            value = yield
            cursor.executemany("insert into user_activity (user_id, layout_id, rating) values (%s,%s,%s)", value)
            conn.commit()


lck = Lock()
con_update, con_get = Pipe()
process_update = Process(target=update_queue, args=(connection, con_update), daemon=True)
process_update.start()

select = getter(con_get)
insert = sender(connection)
next(insert)


@login_required
@csrf_exempt
def index(request):
    if request.method == 'POST':
        user_id = request.user.id
        with lck:
            print(request.POST.items())
            insert.send([(user_id, int(k.split('_', 1)[0]), int(v[0])) for k, v in request.POST.items()])

    layout = []
    for i in range(8):
        with lck:
            layout.append((i, next(select)))

    return render(
        request,
        'index.html',
        context={
            'layout_list': layout,
            'range': range(1, 6),
        }
    )


@csrf_exempt
def logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect('/')
    return render(
        request,
        'logout.html',
    )


@csrf_exempt
def about_view(request):
    with connection.cursor() as cursor:
        cursor.execute(
            "select count(*) from user_activity where activity_ts::date = now()::date"
        )
        count_rating_day = cursor.fetchall()[0][0]
        cursor.execute(
            "select count(*) from user_activity"
        )
        count_rating_all = cursor.fetchall()[0][0]
    return render(
        request,
        'about.html',
        context={
            'count_rating_day': count_rating_day,
            'count_rating_all': count_rating_all,
            'count_graph': Graph.objects.all().count(),
            'count_layout': Layout.objects.all().count(),
            'count_algorithm': AlgorithmLayout.objects.all().count(),
            'count_user': User.objects.all().count(),
        }
    )
