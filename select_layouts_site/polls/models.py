import gzip
import hashlib
import os
import uuid

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from graph_lib.graph import Graph as libGraph


class AlgorithmLayout(models.Model):
    id = models.IntegerField(primary_key=True, null=False, db_column='id')
    name = models.CharField(max_length=32, null=False, db_column='name')
    description = models.TextField(null=False, db_column='description')
    dt = models.DateField(auto_now=True, blank=True, db_column='dt')

    class Meta:
        db_table = 'algorithm_layout'

    def __str__(self):
        return '{}'.format(self.name)


def graph_path(instance, filename):
    return '{}.graphml'.format(instance.id)


class Graph(models.Model):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__graph = None
        self.__invariant = None

    @property
    def graph(self):
        if self.__graph is None:
            self.__graph = (
                libGraph.read_graphml(self.graph_xml.path)
                if os.path.exists(self.graph_xml.path) else
                libGraph.read_graphml(libGraph.get_tmp_file(self.graph_xml.read()))
            )
        return self.__graph

    @property
    def invariant(self):
        if self.__invariant is None:
            self.__invariant = hashlib.sha256(self.graph.invariant).hexdigest()
        return self.__invariant

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, blank=True, db_column='id')
    user_id = models.ForeignKey(to=User, on_delete=models.PROTECT, null=False, blank=True, db_column='user_id')
    graph_invariant = models.CharField(max_length=64, null=False, blank=True, db_column='graph_invariant')
    graph_xml = models.FileField(upload_to=graph_path, null=False, db_column='graph_xml')
    graph_zip = models.BinaryField(null=False, blank=True, db_column='graph_zip')
    dt = models.DateField(auto_now=True, db_column='dt')

    class Meta:
        db_table = 'graph'

    def validate_unique(self, *args, **kwargs):
        super().validate_unique(*args, **kwargs)
        if not self.graph_xml:
            raise ValidationError("upload file")
        for other in self.__class__.objects.filter(graph_invariant=self.invariant):
            if self.graph.isomorphic(other.graph):
                raise ValidationError("isomorphic graph already exists: {}".format(other.id))

    def save(self, *args, **kwargs):
        self.graph_invariant = self.invariant
        self.graph_zip = self.graph.compressed_graph
        super().save(*args, **kwargs)

    def __str__(self):
        return '{}'.format(self.id)


class Layout(models.Model):
    id = models.BigAutoField(primary_key=True, null=False, db_column='id')
    graph_id = models.ForeignKey(to=Graph, to_field='id',
                                 on_delete=models.CASCADE, null=False, db_column='graph_id')
    algorithm_layout_id = models.ForeignKey(to=AlgorithmLayout, to_field='id',
                                            on_delete=models.PROTECT, null=False, db_column='algorithm_layout_id')
    svg = models.BinaryField(null=False, blank=True, db_column='svg')

    def decompress_svg(self):
        return gzip.decompress(self.svg).decode()

    class Meta:
        db_table = 'layout'

    def save(self, *args, **kwargs):
        self.svg = gzip.compress(self.graph_id.graph.draw(self.algorithm_layout_id.id).encode())
        super().save(*args, **kwargs)

    def __str__(self):
        return 'graph: {}, type: {}'.format(self.id, self.algorithm_layout_id)


@receiver(post_delete, sender=Graph)
def drop_file_graph(sender, instance, **kwargs):
    instance.graph_xml.delete(False)
